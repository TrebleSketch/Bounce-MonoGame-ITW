Bounce, a simple rhythm game
=====

Created by Titus Huang using the MonoGame engine. You'll be taping your afternoons away in no time!

Find me on [Twitter](https://www.twitter.com/ILM126)!
