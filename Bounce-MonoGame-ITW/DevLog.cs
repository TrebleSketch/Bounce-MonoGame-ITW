﻿using System;
using System.IO;

namespace Bounce_MonoGame_ITW {

    class DevLog /* v7.3.1 [25/11/2016] */ {

        // http://stackoverflow.com/questions/26432887/c-sharp-access-to-path-denied
        // http://stackoverflow.com/questions/21726088/how-to-get-current-working-directory-path-c
        // http://stackoverflow.com/questions/17118537/an-unhandled-exception-of-type-system-unauthorizedaccessexception-occurred-in

        public static string GetLoggingDirectory() {
            string currentDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Bounce");
            string currentFile = Path.ChangeExtension(currentDirectory, ".log");
            return currentFile;
        }

        public static string GetDebuggingDirectory()
        {
            string currentDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Bounce-debug");
            string currentFile = Path.ChangeExtension(currentDirectory, ".log");
            return currentFile;
        }

        public static string GetCurrentDirectoryFolderWise() {
            string currentDirectoryFolderWise = Directory.GetCurrentDirectory();
            return currentDirectoryFolderWise;
        }

        //public static string GetCurrentContentDirectory() {
        //    string currentContentDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Content");
        //    return currentContentDirectory;
        //}

        //public static string GetCurrentBounceMapDirectory() {
        //    string currentBounceMapDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Content\\BounceMaps");
        //    return currentBounceMapDirectory;
        //}

        /// <summary>
        /// All debugging shall be written to this
        /// </summary>
        /// <param name="text">Debug message</param>
        public static void WriteToFile(string text)
        {
            bool toConsole = true;

            if (toConsole) {
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss ") + "[INFO] " + text);
            } else {
                Console.WriteLine("[WARNING] Debugging Error. Unknown cause of crash");
                throw new System.InvalidOperationException("[WARNING] Debugging Error");
            }

            try {
                using (StreamWriter writer = new StreamWriter(GetDebuggingDirectory(), true)) {
                    if (toConsole) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
                using (StreamWriter writer = new StreamWriter(GetLoggingDirectory(), true)) {
                    if (toConsole) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
            }
            catch (System.UnauthorizedAccessException) {
                WriteToFile("[DEBUG] SHIT, not again. Another Unauthorized Access Exception at StreamWriter, I thought I fixed it!", true, true);
            }
            catch (Exception e) {
                WriteToFile("[DEBUG] StreamWriter encountered an exception: " + e, true, true);
            }
        }

        /// <summary>
        /// All debugging shall be written to this
        /// </summary>
        /// <param name="text">Debug message</param>
        public static void WriteToFile(string text, bool toConsole) {

            if (toConsole) {
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss ") + "[INFO] " + text);
            } else if (!toConsole) {
                // Shouldn't do anything, but I just do this just in case
            } else {
                Console.WriteLine("[WARNING] Debugging Error. Unknown cause of crash");
                throw new System.InvalidOperationException("[WARNING] Debugging Error");
            }

            try {
                using (StreamWriter writer = new StreamWriter(GetDebuggingDirectory(), true)) {
                    if (!toConsole) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[DEBUG] " + text);
                        writer.Close();
                    } else if (toConsole) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
                using (StreamWriter writer = new StreamWriter(GetLoggingDirectory(), true)) {
                    if (!toConsole) { /* Empty */ }
                    else if (toConsole) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
            }
            catch (System.UnauthorizedAccessException) {
                WriteToFile("SHIT, not again. Another Unauthorized Access Exception at StreamWriter, I thought I fixed it!", true, true);
            }
            catch (Exception e) {
                WriteToFile("StreamWriter encountered an exception: " + e, true, true);
            }
        }

        /// <summary>
        /// All debugging shall be written to this
        /// </summary>
        /// <param name="text">Debug message</param>
        public static void WriteToFile(string text, bool toConsole, bool warning) {

            if (toConsole && !warning) {
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss ") + "[INFO] " + text);
            } else if (!toConsole && !warning) {
                // Shouldn't do anything, but I just do this just in case
            } else if (toConsole || !toConsole && warning) {
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss ") + "[WARNING] " + text);
            } else {
                Console.WriteLine("[WARNING] Debugging Error. Unknown cause of crash");
                throw new System.InvalidOperationException("[WARNING] Debugging Error");
            }

            try {
                using (StreamWriter writer = new StreamWriter(GetDebuggingDirectory(), true)) {
                    if (!toConsole && !warning) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[DEBUG] " + text);
                        writer.Close();
                    } else if (toConsole && !warning) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else if (toConsole || !toConsole && warning) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
                using (StreamWriter writer = new StreamWriter(GetLoggingDirectory(), true)) {
                    if (!toConsole) { /* Empty */ }
                    else if (warning) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] " + text);
                        writer.Close();
                    } else if (toConsole && !warning) {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[INFO] " + text);
                        writer.Close();
                    } else {
                        writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + "[WARNING] Debugging Error. Unknonw cause of crash.");
                        writer.Close();
                        throw new System.InvalidOperationException("[WARNING] Debugging Error");
                    }
                }
            }
            catch (System.UnauthorizedAccessException) {
                WriteToFile("[DEBUG] SHIT, not again. Another Unauthorized Access Exception at StreamWriter, I thought I fixed it!", true, true);
            }
            catch (Exception e) {
                WriteToFile("[DEBUG] StreamWriter encountered an exception: " + e, true, true);
            }
        }
    }
}
