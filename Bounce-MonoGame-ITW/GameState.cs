﻿using Microsoft.Xna.Framework;
using EclipsingGameUtils;

namespace Bounce_MonoGame_ITW {

    public enum GameStates {
        empty,
        SplashScreen,
        Menu,
        Game
    }

    public class GameState {

        public GameStates gameStates;

        public GameStates GetCurrentGameState() { return gameStates; }
        public void SetGameState(GameStates state) { gameStates = state; }
        /// <summary>
        /// Initializing GameState, should only be used once
        /// </summary>
        public static GameState InitializeGameState() { GameState gameState; gameState = new GameState(); return gameState; }

        public void Initialize() {
            gameStates = new GameStates();
            gameStates = GameStates.Menu;
            DevLog.WriteToFile(string.Format("========== SCENE CHANGED TO: {0} ==========", gameStates.ToString().ToUpper()));
        }
    }
}
