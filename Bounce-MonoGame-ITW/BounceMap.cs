﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using ManagedBass;
using System.Text;
using System.Linq;
using System.Globalization;
using EclipsingGameUtils;

namespace Bounce_MonoGame_ITW {

    public class BounceMapReader /* v2 [25/11/2016] */ {
        // Using StreamRead to get the data

        enum ImplementedBounceMapData {
            song_name,
            song_artist,
            song_genre,
            song_bpm,
            song_beatsPerBar,
            song_filePath,
            bounceMap_difficulty,
            bounceMap_beatsData,
            bounceMap_noSong
        }

        public List<BounceMapData> bounceMapData = new List<BounceMapData>();
        public static BounceMapReader InitializeBounceMapReader() { BounceMapReader bounceMapReader; bounceMapReader = new BounceMapReader(); return bounceMapReader; }
        ImplementedBounceMapData implementedBounceMapData = new ImplementedBounceMapData();
        ImplementedBounceMapData lastData;

        public void Initialze() {
            implementedBounceMapData = ImplementedBounceMapData.song_name;
            lastData = Enum.GetValues(typeof(ImplementedBounceMapData)).Cast<ImplementedBounceMapData>().Max();
        }

        #region Data
        public void StartReadingFile() {
            DevLog.WriteToFile("===== Starting BounceMap Loader =====");
            ScanDirectory();
        }

        void ScanDirectory() {
            string filePath = Path.Combine(DevLog.GetCurrentDirectoryFolderWise(), "Content\\BounceMaps\\");
            string[] files = Directory.GetFiles(filePath, "*.bounce");
            DevLog.WriteToFile("Bounce Map directory: " + filePath, false);
            DevLog.WriteToFile(files.Length.ToString() + " bounce map files found");
            DevLog.WriteToFile("===== Loading BounceMaps now =====");
            for (var i = 0; i < files.Length; i++) {
                //DevLog.WriteToFile(files[i], false);
                ReadingFileData(files[i]);
            }
            if (bounceMapData.Count > 0)
                DevLog.WriteToFile("===== BounceMap Loading complete =====");
            else
                DevLog.WriteToFile("===== BounceMap Loading failed =====", true, true);
        }

        void ReadingFileData(string currentFile) {
            try {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                //string filePath = Path.Combine(DevLog.GetCurrentDirectoryFolderWise(), "Content\\BounceMaps\\Alphabeat - DJ (Madeon Remix)");
                //string currentFile = Path.ChangeExtension(filePath, ".bounce");
                //DevLog.WriteToFile(string.Format("BounceMap file location: {0}", currentFile), false);

                implementedBounceMapData = ImplementedBounceMapData.song_name;
                // Reset to the first one, yeah mate

                List<string> bounceMapDataString = new List<string>();
                using (StreamReader bounceMapReader = new StreamReader(currentFile)) {
                    string line;

                    while ((line = bounceMapReader.ReadLine()) != null) {
                        //DevLog.WriteToFile("Right after reading line: " + line, false);

                        if (line.StartsWith("//")) {

                        } else {
                            // Usable data

                            //string output = line.Substring(line.IndexOf(':') + 1);

                            switch (implementedBounceMapData)
                            {
                                case ImplementedBounceMapData.song_name:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song Name: " + line, false);
                                    break;
                                case ImplementedBounceMapData.song_artist:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song Artist: " + line, false);
                                    break;
                                case ImplementedBounceMapData.song_genre:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song Genre: " + line, false);
                                    break;
                                case ImplementedBounceMapData.song_bpm:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song BPM: " + line, false);
                                    break;
                                case ImplementedBounceMapData.song_beatsPerBar:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song Beats Per Bar: " + line, false);
                                    break;
                                case ImplementedBounceMapData.song_filePath:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Song file path: " + line, false);
                                    break;
                                case ImplementedBounceMapData.bounceMap_difficulty:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Bounce Map difficulty: " + line, false);
                                    break;
                                case ImplementedBounceMapData.bounceMap_beatsData:

                                    bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Bounce Map Data: " + line, false);
                                    break;
                                case ImplementedBounceMapData.bounceMap_noSong:

                                    if (line == "")
                                        DevLog.WriteToFile("noSong is empty", false);
                                    else
                                        bounceMapDataString.Add(line);
                                    DevLog.WriteToFile("Bounce Map Time When No Song: " + line, false);
                                    break;
                                default:
                                    DevLog.WriteToFile("BounceMapReader: You've asked me to read too many lines! Plus, this should never occur .>.", true, true);
                                    break;
                            }

                            if (implementedBounceMapData != lastData)
                                implementedBounceMapData++;
                        }

                        //var regexToRead = new Regex("^[a-zA-Z0-9_() .-]*$");
                        //if (regexToRead.IsMatch(line)) {
                        //    bounceMapDataString.Add(line);
                        //    DevLog.WriteToFile(line, false);
                        //}
                    }
                }

                    if (bounceMapDataString.Count != 0)
                        if (bounceMapDataString.Count != ((int)lastData + 1)) {
                            DevLog.WriteToFile("bounceMapDataString Count is: " + bounceMapDataString.Count, false);
                            DevLog.WriteToFile("lastData as int is: " + (int)lastData, false);
                            DevLog.WriteToFile("===== BounceMap failed to Load =====");
                            throw new Exception("Not enough bounceMap data was given!");
                        } else {
                            //DevLog.WriteToFile("Amount of data collected : " + bounceMapDataString.Count, false);
                            if (bounceMapDataString[3].ToString() == "") {
                                DevLog.WriteToFile("===== BounceMap failed to Load =====");
                                throw new Exception("No BPM was given!");
                            } else {
                                TransferData(bounceMapDataString);
                                DevLog.WriteToFile("===== BounceMap " + bounceMapData.Count + " Loaded =====");
                            }
                        }
            }
            catch (Exception e) {
                // Let the user know what went wrong
                DevLog.WriteToFile(e.Message, true, true);
            }
        }

        void TransferData(List<string> data) {
            BounceMapData bounceMap = new BounceMapData(
                    data[0],
                    data[1],
                    data[2],
                    ConvertStringToFloat(data[3]),
                    ConvertStringToInt(data[4]),
                    data[5],
                    ConvertStringToInt(data[6]),
                    ConvertStringToArray(data[7]),
                    CheckIfEmptyArray(data[8]));
            bounceMapData.Add(bounceMap);
        }

        static float ConvertStringToFloat(string data) {
            if (data != "") {
                float returnData = float.Parse(data);
                return returnData;
            }
            float returnDataNull = 0f;
            return returnDataNull;
        }
        static int ConvertStringToInt(string data) {
            if (data != "") {
                int returnData = int.Parse(data);
                return returnData;
            }
            int returnDataNull = 0;
            return returnDataNull;
        }
        //static string CheckIfEmpty(string data) { if (data == "") data = "Unknown"; return data; }
        static string[] CheckIfEmptyArray(string data) {
            string[] array;
            if (data != "") {
                array = ConvertStringToArray(data);
            } else
                array = new string[] { };
            return array;
        }
        static string[] ConvertStringToArray(string data) { var array = data.Split(','); return array; }
        // http://stackoverflow.com/questions/5803557/how-do-i-convert-a-string-into-an-array
        #endregion
    }

    public class BounceMapData {
        // What the data should represent

        // Set data
        public string m_name;
        public string m_artist;
        public string m_genre;
        public float m_bpm;
        public int m_beatsPerBar;
        public string m_songFile;
        public int m_difficulty;
        public string[] m_beatsForBounce;
        public string[] m_noSong;

        // Not set data
        
        public string m_localFilePath;
        public string m_fullFilePath;
        public List<TimeSpan> m_noSongLengths;
        public TimeSpan m_lenghtOfAudio;
        public float m_timeBetweenBars;

        public BounceMapData(string name = "Unknown", string artist = "Unknown", string genre = "Unknown", float bpm = 0f, int bpb = 4, string songFile = null, int difficulty = 0, string[] beatsForBounce = null, string[] noSong = null) {

            m_name = name;
            m_artist = artist;
            m_genre = genre;
            m_bpm = bpm;
            m_beatsPerBar = bpb;
            m_songFile = songFile;
            m_difficulty = difficulty;
            m_beatsForBounce = beatsForBounce;
            m_noSong = noSong;

            // Other thingies
            m_localFilePath = string.Concat("BounceMaps/", Path.GetFileNameWithoutExtension(songFile));
            m_fullFilePath = string.Concat(DevLog.GetCurrentDirectoryFolderWise(), "/BounceMaps/", Path.GetFileNameWithoutExtension(songFile));
            m_timeBetweenBars = bpm / 60;
        }
    }

    public class BounceMap {
        public static BounceMap InitializeBounceMap() { BounceMap bounceMap; bounceMap = new BounceMap(); return bounceMap; }
        BounceMapReader bounceMapReader;
        Game1 game1;

        public List<BounceMapData> bounceMapData;
        public List<BeatScroller> beatScroller = new List<BeatScroller>();
        int textBox_width = 210;

        // Beat Scroller Thingy
        public List<Vector2> timeScrollerStartingPositions;
        public Texture2D timeScrollerTexture;
        public Texture2D timeScroller3beatsTexture;
        public Texture2D timeScroller4beatsTexture;
        public Texture2D timeArrowTexture;
        public Vector2 timeArrowPosition;
        public bool isResetYet;

        // UI Thingy
        Texture2D leftBlockRect;
        Texture2D rightBlockRect;
        Texture2D borderRect;
        Texture2D fillRect;
        Texture2D fluffingLeftistRect;
        Texture2D fluffingRightistRect;
        Vector2 leftBlockPos;
        Vector2 rightBlockPos;
        Vector2 borderPos;
        Vector2 fillPos;
        Vector2 fluffingLeftistPos;
        Vector2 fluffingRightistPos;
        Color border = Color.LightGreen;
        Color fill = Color.SaddleBrown;
        int blockWidth = 147;
        int blockHeight = 50;

        public void Initialize(Game1 game, List<BounceMapData> bMD, GraphicsDeviceManager graphics) {
            bounceMapReader = BounceMapReader.InitializeBounceMapReader();
            timeScrollerStartingPositions = new List<Vector2>();
            bounceMapData = bMD;
            game1 = game;
            isResetYet = false;
            timeArrowPosition = new Vector2(Game1.CentreScreen.X, Game1.CentreScreen.Y * 1.65f);

            SetBlockSides(graphics);
            SetBorder(graphics);
            SetFill(graphics);
            DualSidesCover(graphics);
        }
        
        public void LoadScrollerTexture() {

            Texture2D texture;

            if (bounceMapData[game1.songChoice].m_beatsPerBar == 4)
                texture = timeScroller4beatsTexture;
            else if (bounceMapData[game1.songChoice].m_beatsPerBar == 3)
                texture = timeScroller3beatsTexture;
            else
                texture = timeScrollerTexture;

            BeatScroller timeScroller = new BeatScroller(
                texture,
                new Vector2(Game1.CentreScreen.X + (texture.Width / 2), Game1.CentreScreen.Y * 1.75f));
            timeScrollerStartingPositions.Add(timeScroller.m_Position);
            beatScroller.Add(timeScroller);

            double extraBars = ((((bounceMapData[game1.songChoice].m_bpm / (bounceMapData[game1.songChoice].m_beatsPerBar * 2)) * (int)bounceMapData[game1.songChoice].m_lenghtOfAudio.TotalMinutes) + ((bounceMapData[game1.songChoice].m_timeBetweenBars * bounceMapData[game1.songChoice].m_lenghtOfAudio.Seconds) / (bounceMapData[game1.songChoice].m_beatsPerBar * 2))) - 1);
            //// Equation for extraBars => (((bpm / (bpb * 2)) * total minutes) + ((timeBetweenBars * left over seconds) / (bpb * 2))) - 1
            //DevLog.WriteToFile("extraBars: " + extraBars);
            //DevLog.WriteToFile("bars for minutes: " + (bounceMapData[game1.songChoice].m_bpm / 10) * (int)bounceMapData[game1.songChoice].m_lenghtOfAudio.TotalMinutes);
            //DevLog.WriteToFile("bars for seconds: " + (bounceMapData[game1.songChoice].m_timeBetweenBars * bounceMapData[game1.songChoice].m_lenghtOfAudio.Seconds) / 10);
            //DevLog.WriteToFile("timeBetweenBars: " + bounceMapData[game1.songChoice].m_timeBetweenBars);
            //DevLog.WriteToFile("totalMinutes: " + (int)bounceMapData[game1.songChoice].m_lenghtOfAudio.TotalMinutes);
            //DevLog.WriteToFile("seconds: " + bounceMapData[game1.songChoice].m_lenghtOfAudio.Seconds);
            DevLog.WriteToFile("Total scroll bars: " + extraBars + 1, false);

            for (var i = 0; i < extraBars; i++) {
                BeatScroller timeScrollerRepeat = new BeatScroller(
                texture,
                new Vector2(beatScroller[0].m_Position.X + (texture.Width * (1 + i)), beatScroller[0].m_Position.Y));

                timeScrollerStartingPositions.Add(timeScrollerRepeat.m_Position);
                beatScroller.Add(timeScrollerRepeat);
            }
        }

        public void RestartTimeScrollerPosition() {
            var i = 0;
            foreach (BeatScroller timescroller in beatScroller) {
                timescroller.m_Position.X = timeScrollerStartingPositions[i].X;
                i++;
            }
            isResetYet = true;
        }

        public void RemoveTimeScroller(bool reload) {
            List<BeatScroller> TimeScrollerDeathRow = new List<BeatScroller>();

            foreach (BeatScroller scroller in beatScroller) {
                TimeScrollerDeathRow.Add(scroller);
            }

            foreach (BeatScroller scrollerDead in TimeScrollerDeathRow) {
                beatScroller.Remove(scrollerDead);
            }

            if (reload)
                LoadScrollerTexture();
        }

        //public void DrawSquare(GraphicsDeviceManager graphics, SpriteBatch spriteBatch, float length)
        //{
        //    Texture2D rect = new Texture2D(graphics.GraphicsDevice, (int)length, 3);

        //    Color[] data = new Color[(int)length * 3];
        //    for (int i = 0; i < data.Length; ++i) data[i] = Color.Chocolate;
        //    rect.SetData(data);

        //    Vector2 coor = new Vector2 (scrollLine[0].m_Position.X - scrollLine[0].m_Texture.Width / 2, scrollLine[0].m_Position.Y);
        //    spriteBatch.Draw(rect, coor, Color.White);
        //}

        public void Update(float time) {

            if (beatScroller.Count != 0) {
                if (game1.audio.audioState == AudioState.STOPPED) {
                    if (!isResetYet)
                        RestartTimeScrollerPosition();

                    if (InputHandler.IsKeyDownOnce(Game1.stopAudio) && beatScroller[0].m_Position != timeScrollerStartingPositions[0])
                        isResetYet = false;

                    if (InputHandler.IsKeyDownOnce(Keys.PageUp) || InputHandler.IsKeyDownOnce(Keys.PageDown))
                        RemoveTimeScroller(true);
                }

                foreach (BeatScroller scroller in beatScroller) {
                    if (-300 <= scroller.m_Position.X) {
                        if (game1.audio.audioState == AudioState.PLAYING)
                            scroller.m_Velocity.X = -bounceMapData[game1.songChoice].m_timeBetweenBars;
                        else
                            scroller.m_Velocity.X = 0;

                        scroller.m_Position.Y += scroller.m_Velocity.Y * time;
                        scroller.m_Position.X += scroller.m_Velocity.X;
                    }
                }
            }
        }

        // http://stackoverflow.com/questions/15986473/how-do-i-implement-word-wrap
        public string WrapText(SpriteFont spriteFont, string text, float maxLineWidth) {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();
            float lineWidth = 0f;
            float spaceWidth = spriteFont.MeasureString(" ").X;

            foreach (string word in words) {
                Vector2 size = spriteFont.MeasureString(word);

                if (lineWidth + size.X < maxLineWidth) {
                    sb.Append(word + " ");
                    lineWidth += size.X + spaceWidth;
                } else                 {
                    sb.Append("\n" + word + " ");
                    lineWidth = size.X + spaceWidth;
                }
            }

            return sb.ToString();
        }

        public void DrawText(SpriteBatch spriteBatch, int songChoice, Color defaultColour) {
            // Default
            //spriteBatch.DrawString(spriteFont, "", CenterScreen, Color.Black);
            try {
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, WrapText(Game1.font_BradleyHandITC_12px, "NAME: " + bounceMapData[songChoice].m_name, textBox_width), new Vector2(Game1.CentreScreen.X * 2 - 220, 20), defaultColour);
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, WrapText(Game1.font_BradleyHandITC_12px, "ARTIST: " + bounceMapData[songChoice].m_artist, textBox_width), new Vector2(Game1.CentreScreen.X * 2 - 220, 90), defaultColour);
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, "GENRE: " + bounceMapData[songChoice].m_genre, new Vector2(Game1.CentreScreen.X * 2 - 220, 130), defaultColour);
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, "BPM: " + bounceMapData[songChoice].m_bpm, new Vector2(Game1.CentreScreen.X * 2 - 220, 155), defaultColour);
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, "DIFFUCULTY: " + bounceMapData[songChoice].m_difficulty, new Vector2(Game1.CentreScreen.X * 2 - 220, 180), defaultColour);
                spriteBatch.DrawString(Game1.font_BradleyHandITC_12px, "AudioState: " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(game1.audio.audioState.ToString().ToLower()), new Vector2(Game1.CentreScreen.X * 2 - 220, 205), defaultColour);

                spriteBatch.DrawString(Game1.font_SegoUI_15px_Bold, /*"Song Time: " + */game1.audio.currentTimeOfSong + "/" + game1.audio.lengthOfSong, new Vector2(Game1.CentreScreen.X, Game1.CentreScreen.Y * 2 - 55), defaultColour);
            }
            catch (Exception e) {
                DevLog.WriteToFile("Error while trying to draw audio data: " + e, true, true);
            }
        }

        public void DrawArrow(SpriteBatch spriteBatch) {
            spriteBatch.Draw(
                        timeArrowTexture,
                        timeArrowPosition,
                        null,
                        Color.White,
                        0f,
                        new Vector2(
                            timeArrowTexture.Width / 2,
                            timeArrowTexture.Height / 2),
                        1,
                        SpriteEffects.None,
                        0);
        }

        public void DrawBackground(SpriteBatch spriteBatch) {
            spriteBatch.Draw(borderRect, borderPos, Color.White);
            spriteBatch.Draw(fillRect, fillPos, Color.White);
        }

        public void DrawScroller(SpriteBatch spriteBatch, int songChoice) {
            try {
                foreach (BeatScroller scroller in beatScroller) {
                    if (-300 <= scroller.m_Position.X && scroller.m_Position.X <= ((Game1.CentreScreen.X * 2) + (scroller.m_Texture.Width * 2))) {
                        spriteBatch.Draw(
                            scroller.m_Texture,
                            /*new Vector2(
                                CenterScreen.X - (timeScrollerTexture.Width / 2),
                                CenterScreen.Y * 1.75f)*/scroller.m_Position,
                            null,
                            Color.White,
                            0f,
                            scroller.m_Origin,
                            1,
                            SpriteEffects.None,
                            0);
                    }
                }
                spriteBatch.Draw(fluffingLeftistRect, fluffingLeftistPos, Color.White);
                spriteBatch.Draw(fluffingRightistRect, fluffingRightistPos, Color.White);
                spriteBatch.Draw(leftBlockRect, leftBlockPos, Color.White);
                spriteBatch.Draw(rightBlockRect, rightBlockPos, Color.White);
            }
            catch (Exception e) {
                DevLog.WriteToFile("Error while trying to draw audio data: " + e, true, true);
            }
        }

        void SetBlockSides(GraphicsDeviceManager graphics) {
            leftBlockRect = new Texture2D(graphics.GraphicsDevice, blockWidth, blockHeight);
            rightBlockRect = new Texture2D(graphics.GraphicsDevice, blockWidth, blockHeight);

            Color[] data = new Color[blockWidth * blockHeight];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Black;
            leftBlockRect.SetData(data);
            rightBlockRect.SetData(data);

            leftBlockPos = new Vector2(0, (Game1.CentreScreen.Y * 1.75f) - (blockHeight / 2));
            rightBlockPos = new Vector2((Game1.CentreScreen.X * 2) - blockWidth, (Game1.CentreScreen.Y * 1.75f) - (blockHeight / 2));
        }

        void SetBorder(GraphicsDeviceManager graphics) {
            int height = 85;
            int width = graphics.PreferredBackBufferWidth - (blockWidth * 2);

            borderRect = new Texture2D(graphics.GraphicsDevice, width, height);

            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; ++i) data[i] = border;
            borderRect.SetData(data);

            borderPos = new Vector2(Game1.CentreScreen.X - (width / 2), (Game1.CentreScreen.Y * 1.75f) - (height / 2));
        }

        void SetFill(GraphicsDeviceManager graphics) {
            int height = 81;
            int width = graphics.PreferredBackBufferWidth - (blockWidth * 2) - 4;

            fillRect = new Texture2D(graphics.GraphicsDevice, width, height);

            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; ++i) data[i] = fill;
            fillRect.SetData(data);

            fillPos = new Vector2(Game1.CentreScreen.X - (width / 2), (Game1.CentreScreen.Y * 1.75f) - (height / 2));
        }

        void DualSidesCover(GraphicsDeviceManager graphics) {
            int height = 85;
            int width = 2;

            fluffingLeftistRect = new Texture2D(graphics.GraphicsDevice, width, height);
            fluffingRightistRect = new Texture2D(graphics.GraphicsDevice, width, height);

            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; ++i) data[i] = border;
            fluffingLeftistRect.SetData(data);
            fluffingRightistRect.SetData(data);

            fluffingLeftistPos = new Vector2(blockWidth, (Game1.CentreScreen.Y * 1.75f) - (height / 2));
            fluffingRightistPos = new Vector2((Game1.CentreScreen.X * 2) - blockWidth - 2, (Game1.CentreScreen.Y * 1.75f) - (height / 2));
        }
    }

    public class BeatScroller {
        public Texture2D m_Texture;
        public Vector2 m_Position;
        public Vector2 m_Origin;
        public Vector2 m_Velocity;
        public float m_Acceleration;
        public Matrix m_Rotation;

        public BeatScroller(Texture2D texture = null, Vector2 position = new Vector2()) {
            m_Texture = texture;
            m_Position = position;
            if (position != null)
                m_Origin = new Vector2(texture.Width / 2, texture.Height / 2);
            else
                m_Origin = new Vector2();
            m_Velocity = new Vector2();
            m_Acceleration = 0f;
            m_Rotation = Matrix.CreateRotationZ(0);
        }
    }
}
