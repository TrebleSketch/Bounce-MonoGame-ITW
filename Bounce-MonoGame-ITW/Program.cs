﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace Bounce_MonoGame_ITW
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        public static string GameVersionBuildString;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            try {
                if (File.Exists(DevLog.GetDebuggingDirectory())) // DEBUG
                    File.WriteAllText(DevLog.GetDebuggingDirectory(), string.Empty); // Clears DEVLOG!
                if (File.Exists(DevLog.GetLoggingDirectory())) // LOG
                    File.WriteAllText(DevLog.GetLoggingDirectory(), string.Empty); // Clears DEVLOG!
            } catch (Exception) {
                File.Create(DevLog.GetDebuggingDirectory());
                File.Create(DevLog.GetLoggingDirectory());
            }

#if DEBUG

            string getCommitAmountNumber = "rev-list --count HEAD";
            string getLastCommitID = "rev-parse --short HEAD";

            string currentDir = @DevLog.GetCurrentDirectoryFolderWise();
            //DevLog.WriteToFile(currentDir);
            string direct = Path.GetFullPath(Path.Combine(currentDir, @"../../../../.."));
            //DevLog.WriteToFile(direct);

            #region Command: Get Commit Amount in Branch

            string command1 = getCommitAmountNumber;
            Process process1 = new Process();
            ProcessStartInfo processStartInfo1 = new ProcessStartInfo();

            processStartInfo1.CreateNoWindow = true;
            processStartInfo1.FileName = @"C:\Program Files\Git\bin\git.exe";
            processStartInfo1.WorkingDirectory = direct;
            processStartInfo1.Arguments = command1;
            processStartInfo1.RedirectStandardOutput = true;
            processStartInfo1.RedirectStandardError = true;
            processStartInfo1.UseShellExecute = false;
            process1.StartInfo = processStartInfo1;
            process1.Start();

            string error1 = process1.StandardError.ReadToEnd();
            string output1 = process1.StandardOutput.ReadToEnd();
            string errorEdit1 = Regex.Replace(error1, @"\s+", "");
            string outputEdit1 = Regex.Replace(output1, @"\s+", "");

            //DevLog.WriteToFile(command1);
            //if (outputEdit1 != "") { DevLog.WriteToFile("Output: " + outputEdit1); }
            //if (errorEdit1 != "") { DevLog.WriteToFile("Error: " + errorEdit1); }
            #endregion

            #region Command: Get Last Commit ID

            string command2 = getLastCommitID;
            Process process2 = new Process();
            ProcessStartInfo processStartInfo2 = new ProcessStartInfo();

            processStartInfo2.CreateNoWindow = true;
            processStartInfo2.FileName = @"C:\Program Files\Git\bin\git.exe";
            processStartInfo2.WorkingDirectory = direct;
            processStartInfo2.Arguments = command2;
            processStartInfo2.RedirectStandardOutput = true;
            processStartInfo2.RedirectStandardError = true;
            processStartInfo2.UseShellExecute = false;
            process2.StartInfo = processStartInfo2;
            process2.Start();

            string error2 = process2.StandardError.ReadToEnd();
            string output2 = process2.StandardOutput.ReadToEnd();
            string errorEdit2 = Regex.Replace(error2, @"\s+", "");
            string outputEdit2 = Regex.Replace(output2, @"\s+", "");

            //DevLog.WriteToFile(command2);
            //if (outputEdit2 != "") { DevLog.WriteToFile("Output: " + outputEdit2); }
            //if (errorEdit2 != "") { DevLog.WriteToFile("Error: " + errorEdit2); }

            #endregion

            // MAJOR.MINOR.PATCH.COMMIT AMOUNT ON BRANCH
            int major = 0;
            int minor = 4;
            int patch = 0;
            string commitNum = outputEdit1;
            string commitID = outputEdit2;
            string status = "Barely Playable";

            string GameVersionBuild = "v" + major + "." + minor + "." + patch + "." + commitNum + "-" + commitID + " (" + DateTime.Now.ToString("dd/MM/yyyy") + ") [" + status + "]";

#else

            //CommitRepository commit;
            //commit.GetAll("TrebleSketch/Bounce%2DMonoGame%2DITW/);

            int major = 0;
            int minor = 4;
            int patch = 0;
            string commitNum = "22"; // Manually Input
            string commitID = "16b20a0"; // Manually Input
            string status = "Barely Playable";

            string GameVersionBuild = "v" + major + "." + minor + "." + patch + "." + commitNum + "-" + commitID + " (" + DateTime.Now.ToString("dd/MM/yyyy") + ") [" + status + "]";
#endif

            GameVersionBuildString = "Now playing on v" + major + "." + minor + "." + patch + "." + commitNum + " - " + commitID;
            DevLog.WriteToFile("Starting Bounce-MonoGame-ITW " + GameVersionBuild);
            DevLog.WriteToFile("DevLog-Debugging file is located at: " + DevLog.GetDebuggingDirectory());
            DevLog.WriteToFile("DevLog-Logging file is located at: " + DevLog.GetLoggingDirectory());

            using (var game = new Game1())
                game.Run();

            DevLog.WriteToFile("Exiting Game...", true);
        }
    }
}
