﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using EclipsingGameUtils;

namespace Bounce_MonoGame_ITW {
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game {

        #region Variables and Classes
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public Audio audio;
        GameState gameState;
        public BounceMapReader bounceMapReader;
        public BounceMap bounceMap;
        Button button;
        Character character;

        public static Vector2 CentreScreen;
        public static SpriteFont font_SegoUI_10px;
        public static SpriteFont font_BradleyHandITC_12px;
        public static SpriteFont font_SegoUI_15px_Bold;
        public static SpriteFont font_LucidaSansUnicode_20px;
        //public SpriteFont font_LucidaSansUnicode_30px;
        //public SpriteFont font_LucidaSansUnicode_50px;
        public SpriteFont font_LucidaSansUnicode_80px;

        public const Keys stopAudio = Keys.U;
        public const Keys startAudio = Keys.I;
        public const Keys playerJump = Keys.Space;
        public const string GameTitle = "Bounce!";
        public const string TagLine = "a simple rhythm game";
        string GameVersionBuild;

        bool autoStartAudio = false;
        public int songChoice = 3;
        Color defaultColour = Color.White;

        Texture2D menuBannerRect;
        Vector2 menuBannerPos;

        //Rectangle viewingSpot;

        // EmptyKeys UI
        //private int nativeScreenWidth;
        //private int nativeScreenHeight;
        //private UIRoot root;
        //private WindowViewModel viewModel;
        #endregion

        public Game1() {
            graphics = new GraphicsDeviceManager(this) {PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8};
            GameVersionBuild = Program.GameVersionBuildString;
            graphics.PreferredBackBufferWidth = 960;
            graphics.PreferredBackBufferHeight = 540;
            Content.RootDirectory = "Content";
            //graphics.DeviceCreated += graphics_DeviceCreated;
            //graphics.PreparingDeviceSettings += graphics_PreparingDeviceSettings;
            //Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        #region Useless UI Code rn
        //private void Window_ClientSizeChanged(object sender, EventArgs e) {
        //    if (root != null) {
        //        Viewport viewPort = GraphicsDevice.Viewport;
        //        root.Resize(viewPort.Width, viewPort.Height);
        //    }
        //}

        //void graphics_DeviceCreated(object sender, EventArgs e) {
        //    Engine engine = new MonoGameEngine(GraphicsDevice, nativeScreenWidth, nativeScreenHeight);
        //}

        //private void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e) {
        //    nativeScreenWidth = graphics.PreferredBackBufferWidth;
        //    nativeScreenHeight = graphics.PreferredBackBufferHeight;

        //    //graphics.PreferredBackBufferWidth = 1280;
        //    //graphics.PreferredBackBufferHeight = 720;
        //    graphics.PreferredBackBufferHeight = 540;
        //    graphics.PreferredBackBufferWidth = 960;
        //    graphics.PreferMultiSampling = true;
        //    graphics.GraphicsProfile = GraphicsProfile.HiDef;
        //    graphics.SynchronizeWithVerticalRetrace = true;
        //    graphics.PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8;
        //    e.GraphicsDeviceInformation.PresentationParameters.MultiSampleCount = 16;
        //}
        #endregion

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()  {

            DevLog.WriteToFile("========== INITIALIZING GAME ==========");

            CentreScreen = new Vector2(graphics.PreferredBackBufferWidth / 2
                        , graphics.PreferredBackBufferHeight / 2);

            gameState = GameState.InitializeGameState();
            gameState.Initialize();

            bounceMapReader = BounceMapReader.InitializeBounceMapReader();
            bounceMapReader.Initialze();
            bounceMapReader.StartReadingFile();

            audio = Audio.IntializeAudio();
            audio.Initialize();
            if (autoStartAudio && bounceMapReader.bounceMapData.Count > 0 && gameState.gameStates == GameStates.Game) {
                audio.InitializeAudio(bounceMapReader.bounceMapData[songChoice].m_songFile, this);
                audio.CurrentAudioStart();
            } else if (!autoStartAudio && bounceMapReader.bounceMapData.Capacity > 0)
                audio.InitializeAudio(bounceMapReader.bounceMapData[songChoice].m_songFile, this);
            else if (bounceMapReader.bounceMapData.Count == 0) {
                DevLog.WriteToFile("No song loaded, shoot", true, true);
                DevLog.WriteToFile("No song loaded, could not load program. The developer haven't developed a program that can survive without loading a bouncemap", true, true);
                Exit();
            }

            bounceMap = BounceMap.InitializeBounceMap();
            bounceMap.Initialize(this, bounceMapReader.bounceMapData, graphics);

            button = Button.InitializingButton();

            character = new Character();
            character.Initialize();

            IsMouseVisible = true;

            SetMenuBanner();
            //viewingSpot = new Rectangle((int)Game1.CentreScreen.X, (int)(Game1.CentreScreen.Y * 1.75f), (int)Game1.CentreScreen.X, 75);

            base.Initialize();

            DevLog.WriteToFile("========== INITIALIZING GAME COMPLETE ==========");
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            DevLog.WriteToFile("========== LOADING CONTENT ==========");
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font_SegoUI_10px = Content.Load<SpriteFont>("Segoe_UI_10px");
            font_BradleyHandITC_12px = Content.Load<SpriteFont>("Bradley-Hand-ITC-12px");
            font_SegoUI_15px_Bold = Content.Load<SpriteFont>("Segoe_UI_15px_Bold");
            font_LucidaSansUnicode_20px = Content.Load<SpriteFont>("Lucida-Sans-Unicode-20px");
            //font_LucidaSansUnicode_30px = Content.Load<SpriteFont>("Lucida-Sans-Unicode-30px");
            //font_LucidaSansUnicode_50px = Content.Load<SpriteFont>("Lucida-Sans-Unicode-50px");
            font_LucidaSansUnicode_80px = Content.Load<SpriteFont>("Lucida-Sans-Unicode-80px");
            bounceMap.timeScrollerTexture = Content.Load<Texture2D>("TimeScroller");
            bounceMap.timeScroller3beatsTexture = Content.Load<Texture2D>("TimeScroller_3beats");
            bounceMap.timeScroller4beatsTexture = Content.Load<Texture2D>("TimeScroller_4beats");
            bounceMap.timeArrowTexture = Content.Load<Texture2D>("TimeArrow");

            if (gameState.gameStates == GameStates.Game)
                bounceMap.LoadScrollerTexture();

            button.Initialize(graphics);

            for (var i = 0; i < character.texturesToLoad.Count / 2; i++) {

                Texture2D still = Content.Load<Texture2D>(character.texturesToLoad[i * 2]);
                Texture2D animated = Content.Load<Texture2D>(character.texturesToLoad[(i * 2) + 1]);

                Texture2D[] arrayTextures = new Texture2D[] { still, animated };

                CharacterData newCharacter = new CharacterData(arrayTextures);
                character.characterData.Add(newCharacter);
            }

            //SpriteFont font = Content.Load<SpriteFont>("Segoe_UI_15_Bold");
            //FontManager.DefaultFont = Engine.Instance.Renderer.CreateFont(font);
            //Viewport viewport = GraphicsDevice.Viewport;
            //root = new UIRoot(viewport.Width, viewport.Height);
            //viewModel = new WindowViewModel();
            //root.DataContext = viewModel;

            //FontManager.Instance.LoadFonts(Content);

            DevLog.WriteToFile("========== LOADING CONTENT COMPLETE ==========");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent() {
            DevLog.WriteToFile("========== UNLOADING CONTENT ==========");
            audio.ClearContent();
            DevLog.WriteToFile("========== UNLOADING CONTENT COMPLETE ==========");
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (gameState.gameStates == GameStates.Game) {
                AudioUpdates(time);
                bounceMap.Update(time);
                
                if (audio.audioState == AudioState.PLAYING)
                    foreach (CharacterData characters in character.characterData) {
                        characters.Update(time, gameTime, bounceMapReader, this);
                    }
            }

            if (gameState.gameStates == GameStates.Menu)
            {
                if (InputHandler.IsKeyDownOnce(Keys.Space)) // Only can start game with Spacebar
                    button.buttonData[0].isClicked = true;

                foreach (ButtonData butt in button.buttonData) {
                    butt.Update(gameState, bounceMap);
                }
            }

            //if (InputHandler.IsKeyDownOnce(Keys.H))
            //    gameState.gameStates = GameStates.Game;

            //root.UpdateInput(gameTime.ElapsedGameTime.TotalMilliseconds);
            //root.UpdateLayout(gameTime.ElapsedGameTime.TotalMilliseconds);

            base.Update(gameTime);

            InputHandler.Update();
        }

        void AudioUpdates(float time) {
            if (audio.audioState == AudioState.STOPPED)
                if (InputHandler.IsKeyDownOnce(Keys.PageUp) && songChoice < (bounceMapReader.bounceMapData.Count - 1)) {
                    songChoice++;
                    audio.InitializeAudio(bounceMapReader.bounceMapData[songChoice].m_songFile, this);
                } else if (InputHandler.IsKeyDownOnce(Keys.PageDown) && songChoice > 0) {
                    songChoice--;
                    audio.InitializeAudio(bounceMapReader.bounceMapData[songChoice].m_songFile, this);
                }

            if (InputHandler.IsKeyDownOnce(Keys.P) && audio.audioState == AudioState.PLAYING)
                audio.CurrentAudioPause();

            if (InputHandler.IsKeyDownOnce(Keys.O) && audio.audioState == AudioState.PAUSED)
                audio.CurrentAudioUnpause();

            if (InputHandler.IsKeyDownOnce(stopAudio))
                if (audio.audioState == AudioState.PLAYING || audio.audioState == AudioState.PAUSED) {
                    audio.CurrentAudioStop();
                }

            if (InputHandler.IsKeyDownOnce(startAudio) && audio.audioState == AudioState.STOPPED) {
                if (bounceMap.beatScroller[0].m_Position != bounceMap.timeScrollerStartingPositions[0]) {
                    bounceMap.isResetYet = false;
                    bounceMap.RestartTimeScrollerPosition();
                }
                audio.InitializeAudio(bounceMapReader.bounceMapData[songChoice].m_songFile, this);
                audio.CurrentAudioStart();
            }

            audio.Update(time, this);
        }

        //static int NumberKeyCodeToInt(Keys keys) {
        //    string bob = keys.ToString();
        //    int omg = 0;
        //    if (bob.StartsWith("D")) {
        //        string converted = bob.Remove(0, 1);
        //        omg = int.Parse(converted);
        //        return omg;
        //    }
        //    return omg;
        //}

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);
            
            ////////////////
            // Background //
            ////////////////
            spriteBatch.Begin();
            ////////////////

            //root.Draw(gameTime.ElapsedGameTime.TotalMilliseconds);

            //if (Keyboard.GetState().IsKeyDown(Keys.Space))
            //    spriteBatch.DrawString(font_BradleyHandITC_12px, "Space is being pressed", new Vector2(CentreScreen.X - 150, CentreScreen.Y), Color.Black);
            //else
            //    spriteBatch.DrawString(font_BradleyHandITC_12px, "Space isn't being pressed", new Vector2(CentreScreen.X - 150, CentreScreen.Y), Color.Black);

            if (gameState.gameStates == GameStates.Menu) {
                spriteBatch.Draw(menuBannerRect, menuBannerPos, Color.White);
            }

            if (gameState.gameStates == GameStates.Game)
                if (bounceMapReader.bounceMapData.Count > 0)
                {
                    bounceMap.DrawBackground(spriteBatch);
                    bounceMap.DrawText(spriteBatch, songChoice, defaultColour);
                }

            /////////////////////
            spriteBatch.End(); //
            /////////////////////


            ////////////////
            // Foreground //
            ////////////////
            //spriteBatch.GraphicsDevice.RasterizerState.ScissorTestEnable = true;
            //spriteBatch.GraphicsDevice.ScissorRectangle = viewingSpot;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearWrap);
            ////////////////

            if (gameState.gameStates == GameStates.Menu) {
                spriteBatch.DrawString(font_LucidaSansUnicode_80px, GameTitle, new Vector2(CentreScreen.X - (font_LucidaSansUnicode_80px.MeasureString(GameTitle).X / 2), CentreScreen.Y * 0.8f - (font_LucidaSansUnicode_80px.MeasureString(GameTitle).Y / 2)), Color.LightGoldenrodYellow);
                spriteBatch.DrawString(font_LucidaSansUnicode_20px, TagLine, new Vector2(CentreScreen.X - (font_LucidaSansUnicode_20px.MeasureString(TagLine).X / 2), CentreScreen.Y * 1.15f - (font_LucidaSansUnicode_20px.MeasureString(TagLine).Y / 2)), Color.LightGoldenrodYellow);
            }

            if (gameState.gameStates == GameStates.Game) {
                if (character.characterData.Count > 0)
                    character.Draw(spriteBatch);
            }

            spriteBatch.DrawString(font_SegoUI_10px, GameVersionBuild, new Vector2(7, CentreScreen.Y * 2 - (font_SegoUI_10px.MeasureString(GameVersionBuild).Y) - 7), Color.White);
            /////////////////////
            spriteBatch.End(); //
            /////////////////////

            if (gameState.gameStates == GameStates.Game && bounceMapReader.bounceMapData.Count > 0) {
            ////////////////////////
            // BounceMap Scroller //
            ////////////////////////
            //spriteBatch.GraphicsDevice.RasterizerState.ScissorTestEnable = true;
            //spriteBatch.GraphicsDevice.ScissorRectangle = viewingSpot;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicWrap, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            ////////////////

            

                //// Rectangle over the whole game screen
                //var screenArea = new Rectangle(0, 0, 960 / 2, 540 / 2);

                //// Calculate the current offset of the texture
                //// For this example I use the game time
                ////var offset = (int)gameTime.TotalGameTime.TotalMilliseconds;

                //int velocity = 0;
                //int offset = 0;
                //int time = (int)gameTime.ElapsedGameTime.TotalSeconds;

                //if (audio.audioState == AudioState.PLAYING)
                //    velocity = (int)-bounceMap.bounceMapData[songChoice].m_timeBetweenBars;
                //else
                //    velocity = 0;

                //offset += velocity * time;

                //// Offset increases over time, so the texture moves from the bottom to the top of the screen
                //var destination = new Rectangle(offset, 0, bounceMap.scrollLine[0].m_Texture.Width, bounceMap.scrollLine[0].m_Texture.Height);

                //spriteBatch.Draw(
                //    bounceMap.scrollLine[0].m_Texture,
                //    screenArea,
                //    destination,
                //    Color.White);

                bounceMap.DrawScroller(spriteBatch, songChoice);
                bounceMap.DrawArrow(spriteBatch);

            /////////////////////
            spriteBatch.End(); //
            /////////////////////
            }

            /////////////
            // Buttons //
            /////////////
            spriteBatch.Begin(SpriteSortMode.Immediate);
            /////////////

            if (gameState.gameStates == GameStates.Menu)
                foreach (ButtonData butt in button.buttonData) {
                    butt.Draw(spriteBatch);
                }

            spriteBatch.End();


            base.Draw(gameTime);
        }

        void SetMenuBanner() {
            int height = 125;
            int width = 450;

            menuBannerRect = new Texture2D(graphics.GraphicsDevice, width, height);

            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Chocolate;
            menuBannerRect.SetData(data);

            menuBannerPos = new Vector2(CentreScreen.X - (width / 2), (CentreScreen.Y * 0.8f) - (height / 2));
        }

        //public void DrawSquare(GraphicsDeviceManager graphics, SpriteBatch spriteBatch) {
        //    Texture2D rect = new Texture2D(graphics.GraphicsDevice, 400, 150);

        //    Color[] data = new Color[400 * 150];
        //    for (int i = 0; i < data.Length; ++i) data[i] = Color.Chocolate;
        //    rect.SetData(data);

        //    Vector2 coor = new Vector2(CentreScreen.X, CentreScreen.Y * 0.8f);
        //    spriteBatch.Draw(rect, coor, Color.White);
        //}
    }
}
