﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using EclipsingGameUtils;

namespace Bounce_MonoGame_ITW {

    public enum CharacterState {
        Still,
        Animating
    }

    enum CharacterAnimationType {

    }

    public class CharacterData {
        public string m_name;

        public Vector2 m_position;
        public Vector2 m_velocity;
        public Vector2 m_size;
        public Vector2 m_origin;
        public float m_scale;
        public Texture2D[] m_sprites = new Texture2D[2];
        // 0 = Still
        // 1 = Animating

        Vector2 m_originalPosition;
        bool isJumping;
        float originalVelocity;
        TimeSpan jumpSpeed;

        public CharacterData(Texture2D[] sprites = null) {
            if (sprites == null)
                DevLog.WriteToFile("No Character Sprite loaded!", true, true);
            m_sprites = sprites;
            if (sprites != null) {
                string name = m_sprites[0].Name.Replace("Character/", "");
                m_name = name;
            }
            //DevLog.WriteToFile("Sprite Name: " + m_name, false);
            m_position = new Vector2(Game1.CentreScreen.X, Game1.CentreScreen.Y/* - 50*/);
            m_velocity = new Vector2(0, 0);
            m_size = new Vector2(m_sprites[0].Width, m_sprites[0].Height);
            m_origin = new Vector2(
                m_size.X / 2,
                m_size.Y / 2);
            m_scale = 1;

            m_originalPosition = m_position;
        }
        
        public void Update(float time, GameTime gameTime, BounceMapReader bounceMapReader, Game1 game) {
            if (InputHandler.IsKeyDownOnce(Game1.playerJump)) {
                originalVelocity = -15f;
                isJumping = true;
                jumpSpeed = gameTime.TotalGameTime;
            }

            if (isJumping) {
                // (float)Math.Pow(1000, time)
                //DevLog.WriteToFile("velocity: " + m_velocity.Y, false);
                //DevLog.WriteToFile("position: " + m_position.Y, false);

                TimeSpan currentTime = gameTime.TotalGameTime - jumpSpeed; 

                if (m_position.Y > (Game1.CentreScreen.Y * 0.9f) && m_velocity.Y <= 0)
                    m_velocity.Y += (originalVelocity * (float)Math.Pow(1.65f, currentTime.TotalSeconds));
                else if (m_position.Y < (Game1.CentreScreen.Y * 0.9f)) {
                    if (m_velocity.Y < 0)
                        m_velocity.Y = 0;
                    m_velocity.Y += ((-originalVelocity * 30) * (float)Math.Pow(0.4f, currentTime.TotalSeconds));
                }
                m_position.Y += m_velocity.Y * time;
                //m_position.X += m_velocity.X;

                if (m_position.Y > m_originalPosition.Y) {
                    m_position.Y = m_originalPosition.Y;
                    m_velocity.Y = 0;
                    originalVelocity = 0;
                    isJumping = false;
                }
            }
        }
    }

    public class Character {

        public static Character InitializeCharacter() { Character character; character = new Character(); return character; }

        class CharacterAnimation {
            public float m_timer;
            public float m_interval;

            public int m_frameCount;
            public int m_currentFrame;

            public int m_frameWidth;
            public int m_frameHeight;

            public Rectangle m_currentSprite;
            public Vector2 m_size;
            public Vector2 m_position;
            public CharacterAnimationType m_myType;
        }
        List<CharacterAnimation> characterAnimation;
        public List<CharacterData> characterData;

        List<Texture2D> loadedTextures;
        public List<string> texturesToLoad;

        public void Initialize() {
            characterData = new List<CharacterData>();
            characterAnimation = new List<CharacterAnimation>();
            loadedTextures = new List<Texture2D>();
            texturesToLoad = new List<string>();
            StartTextureLoad();
        }

        void StartTextureLoad() {
            DevLog.WriteToFile("===== Starting Character Data Loader =====");
            ScanDirectory();
        }

        void ScanDirectory() {
            string filePath = Path.Combine(DevLog.GetCurrentDirectoryFolderWise(), "Content\\Character\\");
            string[] files = Directory.GetFiles(filePath, "*.character");

            if (files.Length == 0) {
                DevLog.WriteToFile("No Character Data files exist, stopping character data loading", true, true);
                DevLog.WriteToFile("===== Character Data Loading failed =====", true, true);
                return;
            }

            DevLog.WriteToFile("Character Data directory: " + filePath, false);

            if (files.Length < 1)
                DevLog.WriteToFile(files.Length.ToString() + " character data files found");
            else
                DevLog.WriteToFile(files.Length.ToString() + " character data file found");

            DevLog.WriteToFile("===== Loading Character Data Now =====");

            for (var i = 0; i < files.Length; i++) {
                //DevLog.WriteToFile(files[i], false);
                ReadingFileData(files[i]);
            }

            if (texturesToLoad.Count % 2 == 0)
                DevLog.WriteToFile("===== Character Data Loading complete =====");
            else
                DevLog.WriteToFile("===== Character Data Loading failed =====", true, true);

            //if (filesAnimated.Length != filesStill.Length) {
            //    DevLog.WriteToFile("There isn't a still image file for ever animated image file", true, true);
            //    return;
            //}
        }

        void ReadingFileData(string currentFile) {
            try {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                //string filePath = Path.Combine(DevLog.GetCurrentDirectoryFolderWise(), "Content\\BounceMaps\\Alphabeat - DJ (Madeon Remix)");
                //string currentFile = Path.ChangeExtension(filePath, ".bounce");
                DevLog.WriteToFile(string.Format("Character Data file location: {0}", currentFile), false);

                using (StreamReader reader = new StreamReader(currentFile)) {
                    string line;
                    while ((line = reader.ReadLine()) != null) {

                        if (line.StartsWith("//")) {

                        } else {
                            string contentFile = string.Format("Character/{0}", line);
                            texturesToLoad.Add(contentFile);
                            DevLog.WriteToFile(contentFile, false);
                        }
                    }
                }
                // http://stackoverflow.com/questions/5796383/insert-spaces-between-words-on-a-camel-cased-token
                // http://stackoverflow.com/questions/5027853/how-to-remove-a-part-of-string-effectively
                if (texturesToLoad.Count % 2 == 0)
                    DevLog.WriteToFile("===== Character Data of [" + System.Text.RegularExpressions.Regex.Replace(texturesToLoad[texturesToLoad.Count - 1].ToString().Replace("Character/animated-", ""), "(\\B[A-Z])", " $1") + "] Loaded =====");
            }
            catch (System.Exception e)
            {
                // Let the user know what went wrong
                DevLog.WriteToFile(e.Message, true, true);
            }
        }


        public void Update() {

        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(
                characterData[0].m_sprites[0],
                /*new Vector2(
                    CenterScreen.X - (timeScrollerTexture.Width / 2),
                    CenterScreen.Y * 1.75f)*/characterData[0].m_position,
                null,
                Color.White,
                0f,
                characterData[0].m_origin,
                1,
                SpriteEffects.None,
                0);
        }
    }
}
