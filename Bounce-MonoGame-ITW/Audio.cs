﻿using ManagedBass;
using System;

namespace Bounce_MonoGame_ITW {

    public enum AudioState {
        PLAYING,
        PAUSED,
        STOPPED
    }

    public class Audio {

        public static Audio IntializeAudio() { Audio audio; audio = new Audio(); return audio; }
        public int audioStream;

        string audioFile;

        long LENGTH0;
        double LENGTH;
        TimeSpan timeInSeconds;
        public string lengthOfSong;

        long BYTESREAD;
        double INAUDIO;
        TimeSpan currentTime;
        public string currentTimeOfSong;

        public AudioState audioState;

        /// <summary>
        /// Initializes the Audio System
        /// </summary>
        /// <param name="volume">Sets the volume of the audio system</param>
        public void Initialize(float volume) {
            if (Bass.Init()){
                if (volume <= 1 || volume >= 0)
                    Bass.Volume = volume;
                else
                    DevLog.WriteToFile("Volume is over the limits, please select a number above 0 and below 1, thanks", true, true);
            } else DevLog.WriteToFile("BASS could not be initialized!", true, true);
            audioState = AudioState.STOPPED;
        }

        /// <summary>
        /// Initializes the Audio System
        /// </summary>
        public void Initialize()
        {
            if (Bass.Init()) {

            }
            else DevLog.WriteToFile("BASS could not be initialized!", true, true);
            audioState = AudioState.STOPPED;
        }

        public int InitializeAudio(string audioFileInput, Game1 game1) {
            Bass.StreamFree(audioStream);
            int returningData = 0;
            audioStream = 0;
            if (audioStream == 0) {
                audioFile = audioFileInput;
                string accessFromExe = string.Concat("Content/BounceMaps/" + audioFile);
                // Create a stream from a file
                audioStream = Bass.CreateStream(accessFromExe, 0, 0, BassFlags.Prescan);
                LENGTH0 = Bass.ChannelGetLength(audioStream, PositionFlags.Bytes);
                LENGTH = Bass.ChannelBytes2Seconds(audioStream, LENGTH0);
                timeInSeconds = TimeSpan.FromSeconds(LENGTH);
                lengthOfSong = string.Format("{0:D2}:{1:D2}.{2:D2}",
                    timeInSeconds.Minutes,
                    timeInSeconds.Seconds,
                    timeInSeconds.Milliseconds);
                if (game1.bounceMapReader.bounceMapData[game1.songChoice].m_lenghtOfAudio == TimeSpan.Zero)
                    game1.bounceMapReader.bounceMapData[game1.songChoice].m_lenghtOfAudio = timeInSeconds;
                returningData = audioStream;
            }
            //DevLog.WriteToFile(returningData.ToString());
            return returningData;
        }

        public void CurrentAudioStart() {
            if (audioStream != 0) {
                Bass.ChannelPlay(audioStream); // Play the stream
                audioState = AudioState.PLAYING;
                DevLog.WriteToFile("Started audioStream, now playing: " + System.IO.Path.GetFileNameWithoutExtension(audioFile));
            }
            // Error playing the stream
            else DevLog.WriteToFile("Error: " + Bass.LastError + "!", true, true);
        }

        public void CurrentAudioPause() {
            if (audioStream != 0){
                Bass.ChannelPause(audioStream);
                audioState = AudioState.PAUSED;
            } else DevLog.WriteToFile("Nothing to pause", true, true);
        }

        public void CurrentAudioUnpause() {
            if (audioStream != 0) {
                Bass.ChannelPlay(audioStream);
                audioState = AudioState.PLAYING;
            } else DevLog.WriteToFile("Nothing to unpause", true, true);
        }

        public void CurrentAudioStop() {
            // Free the stream
            //Bass.StreamFree(audioStream);
            // Free current device.
            //Bass.Free();

            Bass.ChannelStop(audioStream);
            audioState = AudioState.STOPPED;
            DevLog.WriteToFile("Stopping audioStream");
        }

        public void Update(float time, Game1 game) {
            if (Bass.ChannelIsActive(audioStream) == PlaybackState.Stopped && audioState != AudioState.STOPPED) {
                Bass.ChannelStop(audioStream);
                audioState = AudioState.STOPPED;
                DevLog.WriteToFile("audioStream finished playing");
            }

            BYTESREAD = Bass.ChannelGetPosition(audioStream, PositionFlags.Bytes);
            INAUDIO = Bass.ChannelBytes2Seconds(audioStream, BYTESREAD);
            currentTime = TimeSpan.FromSeconds(INAUDIO);
            if (game.bounceMap.isResetYet && audioState == AudioState.STOPPED)
                currentTime = new TimeSpan(0, 0, 0, 0, 0);
            currentTimeOfSong = string.Format("{0:D2}:{1:D2}.{2:D2}",
                currentTime.Minutes,
                currentTime.Seconds,
                currentTime.Milliseconds);
        }

        public void ClearContent() {
            Bass.ChannelStop(audioStream);
            Bass.Free();
            DevLog.WriteToFile("Finished freeing audio engine");
        }
    }
}
