﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using EclipsingGameUtils;

namespace Bounce_MonoGame_ITW
{
    class Button {
        public static Button InitializingButton() { Button button; button = new Button(); return button; }
        public List<ButtonData> buttonData = new List<ButtonData>();

        public void Initialize(GraphicsDeviceManager graphics) {
            ButtonData button = new ButtonData(
                "Start",
                Game1.font_BradleyHandITC_12px,
                Color.White,
                GameStates.Game,
                new Vector2(120, 35),
                new Vector2(Game1.CentreScreen.X, Game1.CentreScreen.Y * 1.4f),
                Color.Purple,
                graphics);

            buttonData.Add(button);
        }
    }

    class ButtonData {
        public GameStates m_gameState;
        InputHandler m_input;
        GraphicsDeviceManager m_graphics;

        public string m_text;
        public SpriteFont m_spriteFont;
        public Texture2D m_background;
        //public Texture2D[] m_textures = new Texture2D[3];
        // Normal
        // Hover
        // Clicked
        public Vector2 m_position;
        public Vector2 m_size;
        public Vector2 m_origin;
        public Rectangle m_rect;
        public Color m_colour;
        public Color m_textColour;

        public bool isClicked;
        public bool isHover;
        bool needToChangeColour;
        bool toDefaultColour;
        Color defaultColour;
        Color defaultTextColour;
        Color isClickedColour;
        Color isHoveredColour;

        public ButtonData(string text = "", SpriteFont spritefont = null, Color? textColour = null, GameStates gameState = GameStates.SplashScreen, /*Texture2D[] textures = null,*/Vector2 size = new Vector2(), Vector2 position = new Vector2(), Color? colour = null, GraphicsDeviceManager graphics = null) {
            m_gameState = gameState;
            m_input = new InputHandler();
            m_graphics = graphics;

            m_text = text;
            m_spriteFont = spritefont;
            //m_textures = textures;
            m_position = position;
            m_size = size;
            m_origin = new Vector2(size.X / 2, size.Y / 2);
            m_rect = new Rectangle(
                (int)(m_position.X - m_origin.X),
                (int)(m_position.Y - m_origin.Y),
                (int)m_size.X,
                (int)m_size.Y);
            m_colour = colour ?? Color.White;
            m_textColour = textColour ?? Color.Black;

            defaultColour = m_colour;
            defaultTextColour = m_textColour;

            isHoveredColour = new Color(m_colour.R + 40, m_colour.G + 40, m_colour.B + 40, m_colour.A);
            isClickedColour = new Color(m_colour.R - 40, m_colour.G - 40, m_colour.B - 40, m_colour.A/* - 90*/);

            //DevLog.WriteToFile("isClickedColour: " + isClickedColour.ToString());
            //DevLog.WriteToFile("isHoveredColour: " + isHoveredColour.ToString());

            SetMenuBanner(m_colour);
            //m_text = WrapText(m_spriteFont, m_text, m_size.X);
        }

        void SetMenuBanner(Color colour) {
            int height = (int)m_size.Y;
            int width = (int)m_size.X;

            m_background = new Texture2D(m_graphics.GraphicsDevice, width, height);

            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; ++i) data[i] = colour/*Color.Chocolate*/;
            m_background.SetData(data);
        }

        // http://stackoverflow.com/questions/15986473/how-do-i-implement-word-wrap
        //public string WrapText(SpriteFont spriteFont, string text, float maxLineWidth) {
        //    string[] words = text.Split(' ');
        //    StringBuilder sb = new StringBuilder();
        //    float lineWidth = 0f;
        //    float spaceWidth = spriteFont.MeasureString(" ").X;

        //    foreach (string word in words) {
        //        Vector2 size = spriteFont.MeasureString(word);

        //        if (lineWidth + size.X < maxLineWidth) {
        //            sb.Append(word + " ");
        //            lineWidth += size.X + spaceWidth;
        //        } else {
        //            sb.Append("\n" + word + " ");
        //            lineWidth = size.X + spaceWidth;
        //        }
        //    }
        //    return sb.ToString();
        //}

        public void Update(GameState gameState, BounceMap bounceMap) {
            toDefaultColour = false;

            if (m_input.MouseInRectangle(m_rect)) {
                isHover = true;
                isClicked = false;

                if (m_input.MouseButtonClickedOnce(MouseButton.Left))
                    isClicked = true;
                if (m_input.MouseButtonClickedOnce(MouseButton.Right))
                    isClicked = true;

                if (m_colour != isHoveredColour && !needToChangeColour)
                    needToChangeColour = true;
                if (m_colour == isHoveredColour && !needToChangeColour && isClicked)
                {
                    needToChangeColour = true;
                    DevLog.WriteToFile("Is changing colour via clicks", false);
                }
            } else if (isClicked) // From outside influence
                ChangeColours(gameState, bounceMap);
            else {
                if (isHover)
                    toDefaultColour = true;
                isHover = false;
                isClicked = false;
            }

            if (needToChangeColour && isHover)
                ChangeColours(gameState, bounceMap);
            else if (!needToChangeColour && !isHover && toDefaultColour)
                ChangeColours(gameState, bounceMap);           

            //if (m_gameState != GameStates.empty && isClicked)
            //{
            //    gameState.gameStates = m_gameState;
            //    DevLog.WriteToFile("gameState should've changed");
            //}
        }

        void ChangeColours(GameState gameState, BounceMap bounceMap) {
            if (!toDefaultColour) {
                //m_colour = isClicked ? isHoveredColour : isClickedColour;
                if (isClicked) {
                    m_colour = isClickedColour;
                    gameState.gameStates = m_gameState;
                    if (m_gameState == GameStates.Game) {
                        DevLog.WriteToFile(string.Format("========== SCENE CHANGED TO: {0} ==========", m_gameState.ToString().ToUpper()));
                        bounceMap.LoadScrollerTexture();
                    } else if (m_gameState != GameStates.Game) {
                        bounceMap.RemoveTimeScroller(false);
                    }
                } else if (isHover)
                    m_colour = isHoveredColour;

                //DevLog.WriteToFile("Changed to: " + m_colour.ToString(), false);
                //m_textColour = isClicked ? new Color(m_textColour.R * 1.4f, m_textColour.G * 1.4f, m_textColour.B * 1.4f, m_textColour.A) : new Color(m_textColour.R, m_textColour.G, m_textColour.B, m_textColour.A * 0.7f);
            } else {
                m_colour = defaultColour;
                //m_textColour = defaultTextColour;
            }

            SetMenuBanner(m_colour);

            isClicked = false;
            isHover = false;
            needToChangeColour = false;
            toDefaultColour = false;
        }

        //public static Point Origin(Vector2 v) {
        //    //better! does correctly round the values
        //    return new Point(Convert.ToInt32(v.X), Convert.ToInt32(v.Y));
        //}

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(m_background, m_position - m_origin, Color.White);
            spriteBatch.DrawString(m_spriteFont, m_text, new Vector2(m_position.X - (m_origin.X / 3), m_position.Y - (m_origin.Y / 2)), m_textColour);
        }
    }
}
